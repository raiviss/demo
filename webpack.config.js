const path = require('path');
const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = function(env, argv) {
	var PROD = argv.mode == 'production';
	console.log('PROD :', PROD);

	const prodOptimization = {
		splitChunks: {
			chunks: 'all'
		},
		minimize: true,
		minimizer: [
			new TerserPlugin({
				extractComments: false,
				cache: true,
				parallel: true,
				sourceMap: false,
				terserOptions: {
					safari10: true,
					extractComments: 'all',
					compress: {
						keep_fnames: false,
						drop_console: true
					}
				}
			})
		]
	};

	return {
		
		devServer: {
			publicPath: path.join(__dirname, 'dist'),
			//contentBase: "./",
			hot: true,
			//contentBase: path.join(__dirname, 'dist'),
			//compress: true,
			port: 8080
		  },
		entry: ['./src/index.js'],
		devtool: PROD ? false : 'source-map',
		optimization: PROD ? prodOptimization : {},
		module: {
			rules: [
				{
					test: /\.js$/,
					loader: 'babel-loader',
					query: {
						presets: ['@babel/preset-env', '@babel/preset-react'],
						plugins: ['@babel/plugin-proposal-class-properties']
					},
					include: [path.resolve(__dirname, 'src')]
				},
				{
					test: /\.css$/,
					use: ['style-loader', 'css-loader']
				},
				{
					test: /\.less$/,
					exclude: /node_modules/,
					use: [
						{
							loader: 'style-loader'
						},
						{
							loader: 'css-loader'
						},
						{
							loader: 'less-loader'
						}
					]
				},
				{
					test: /\.(ttf|eot|svg|woff|woff2)$/,
					loader: 'file-loader',
					options: {
						name: '../fonts/[name].[ext]'
					}
				}
			]
		},
		output: {
			filename: PROD ? '[name].[contenthash].js' : '[name].js',
			path: path.resolve(__dirname, 'dist'),
			publicPath: '/dist/'
		},
		plugins: [
			new webpack.DefinePlugin({
				isProduction: JSON.stringify(PROD),
			}),
			new CleanWebpackPlugin(),
			new HtmlWebpackPlugin({
				filename: '../index.html',
				template: './template.html'
			})
		]
	};
};
