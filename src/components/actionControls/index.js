import React from 'react';
import style from './style.css';

export default ({cancelPathActive, cancelPathHandler}) => (
	<div className="actionButtonWrap">
	  {cancelPathActive ? (<button className="actionMapBtt" onClick={cancelPathHandler}> CANCEL P</button>) : ""}
	  <button className="actionMapBtt">SALVAGE</button>
	</div>
  )
