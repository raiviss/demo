import { Route, Switch } from 'react-router' // react-router v4/v5
import React from 'react';
import Chat from '../../chatApp/index';
import CurrentPosition from '../currentPosition/index';
import TargetDetails from '../../targetDetails/container';
import css from './style.css';

const component = ({push}) => (
	<div className="contentOverlay">
	  {/*<Chat />*/}
	  <Switch>
		<Route path="/game/stats">
		  <div>STATS</div>
		</Route>
		<Route path="/game/inventory">
			<div>INV</div>
		</Route>
		<Route path="/game/crafting">
			<div>CRAFT</div>
		</Route>
		<Route path="/game">
			<Switch>
		  	<Route path="/game/targetplayer">
				<TargetDetails />
			</Route>
			<Route path="/game/targetnpc">
				<TargetDetails />
			</Route>
		  </Switch>
			<CurrentPosition />
		</Route>
	  </Switch>
	</div>
  )

  export default component;