import React from 'react';
import css from './style.css';

const component = ({coords, combatRange, areaType}) => (
	<div className="squareInfo">
	  <div>Location: {coords}</div> 
	  <div>{areaType} </div>
	  <div><img src="/images/staticons/ranged.png" />{combatRange} </div>
	</div>
)

export default component;