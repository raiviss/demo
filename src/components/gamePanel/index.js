import React from 'react';
import ContentOverlay from '../contentOverlay/index';
import ActionControls from '../../actionControls/container';
import TopPanel from '../topPanel/index';
import style from './style.css';
import {
	
	Link
  } from "react-router-dom";

export default ({push}) => (
	<div className="wrap">
			<div className="topPanel"><TopPanel /></div>
			<div className="mapPanel">Map View
				<div>mercs</div>
			</div>
			<div className="mapOverlay pointerEvents">
				<ContentOverlay />
			</div>
			<div className="bottomPanel pointerEvents">
				<button onClick={()=>{push('/game')}}>map</button>
				<button onClick={()=>{push('/game/stats')}}>stats</button>
				<button onClick={()=>{push('/game/inventory')}}>inventory</button>
				<button onClick={()=>{push('/game/crafting')}}>crafting</button>
				<button onClick={()=>{}}>chat</button>
			</div>
		</div>
	);