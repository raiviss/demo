import React from 'react'
import css from './app.css';
import { Route, Switch } from 'react-router' // react-router v4/v5
import Login from '../user/loginContainer';
import ActionControls from '../actionControls/container';
import GamePanel from '../gamePanel/container';

const App = ({push}) => (
  <div>
    <Switch>
      <Route path="/register">
        <div className="wrap">
        <div className="pointerEvents">
        </div>
        </div>
      </Route>
      <Route path="/game">
        <GamePanel push={push} />
        <div className="actionControls pointerEvents">
          <ActionControls  />
        </div>
      </Route>
      <Route path="/">
        <div className="wrap">
          <div className="pointerEvents">
            <Login />
          </div>
        </div>
      </Route>
    </Switch>
  </div>
)

export default App