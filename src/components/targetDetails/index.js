import React from 'react';
import PropTypes from 'prop-types'
import Scout from './scout';
import Fight from './fight';
import style from './style.css';

const displayRange = (number) => {

	switch(number) {
		case 1: return 'Very Close';
		case 2: return 'Close';
		case 3: return 'Medium';
		case 4: return 'Far';
		case 5: return 'Very Far';
	}
}

class Chat extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
		};
	  }

	render () {

		const { stage } = this.props;

		if(stage === 'scout') {

			return (<Scout {...this.props} displayRange={displayRange} />);
		} else if (stage === 'fight') {
			return <Fight {...this.props} displayRange={displayRange} />
		} else if (stage === 'result') {
		//	return <Result />
		}
		return (<div></div>);
	}
}

Chat.propTypes = {
	stage: PropTypes.string.isRequired
}
  
export default Chat