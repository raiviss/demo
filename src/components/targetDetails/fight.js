import React from 'react';
import PropTypes from 'prop-types'

class Fight extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
		};
	}

	render () {

		var {displayRange, attackHandler, Attack,Defense,	
			EncounterID,Id,	type,	Name,	Position,	Position2, distance, typeDisplay, fightData} = this.props;

		return  (<div className="targetDetails">
			<div>
				<div className="fightBatWrap"><div className="fightBar"></div></div>
			</div>
			<div className="fightResults">
				<div
				dangerouslySetInnerHTML={{
					__html: fightData.text
				}}></div>
			</div>
		</div>);
	}
	
}

Fight.propTypes = {
	Name: PropTypes.string.isRequired
}
  
export default Fight