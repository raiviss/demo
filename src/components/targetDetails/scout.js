import React from 'react';
import ReactSlider from 'react-slider';
import style from './style.css';

var value = 3;

class TargetDetails extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			tactics: '1'
		};
	  }
	
	  isSelected(value) {

		return (value === this.state.tactics ? 'selected' : 'false');
	  }

	  changeSelect(event){
		this.setState({tactics: event.target.value});
	}

	render () {

		const {displayRange, attackHandler, Attack,Defense,
			scoutIntelHtml,	EncounterID,Id,	type,	Name,	
			Position,	Position2, distance, typeDisplay} = this.props;
 
		return (<div className="targetDetails">
			<div><h3>{Name}</h3></div>
			<div className="details">
				<div>Type: {type}</div>
				<div>Attack Power: {Attack}</div>
				<div>Defense Power:{Defense}</div>
			</div>
			<div>
			<div>Area Details: {typeDisplay} ({Position}.{Position}) {distance} range</div>
			<p></p>
				
				{(type === 'player' ? (<div className="playerScoutDetails"
							dangerouslySetInnerHTML={{
								__html: scoutIntelHtml
							}}></div>) : '')}
				<p></p>
				<div className="attackRangeText"><strong>Attack Range</strong></div>
				<div className="rangeSliderWrap">
					<ReactSlider
						min={1}
						max={5}
						defaultValue={[3]}
						className="range-slider"
						thumbClassName="range-thumb"
						trackClassName="range-track"
						renderThumb={(props, state) => {
							value = state.valueNow;
							return (<div {...props}>{displayRange(state.valueNow)}</div>)}}
					/>
				</div>
				{(type === 'player' ? (<div className="attackTactics">
					<select name="tactics" onChange={(ev)=> {this.changeSelect(ev)}} value={this.state.value}>
						<option value="1">Close</option>
						<option value="2">Scattered</option>
						<option value="3">Flanking</option>
					</select>
				</div>) : '')}
				<div><button onClick={()=>{attackHandler(Id, type, value, this.state.tactics)}}>Attack</button></div>
			</div>
			</div>);

		}
	}

export default TargetDetails;