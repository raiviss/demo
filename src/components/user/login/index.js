import React from 'react'
import PropTypes from 'prop-types'
import css from './style.css';

class Login extends React.Component {
	constructor(props) {
	  super(props);
	  this.state = {
		email: '',
		password: ''
	  };
	}

	updateInputValue(field, evt) {

		this.setState({[field]: evt.target.value});
		
		evt.preventDefault();
	}
	
	render (){
		
		return (
				<div className="login">
					<div className="dawnLogoLogin">
						<img src="/images/dawn2055_logo.png" alt="Dawn" />
					</div>
					<div className="loginInputWrap">
						<form method="post">
							<div>
								<div>Email</div>
								<input 
									onChange={evt => this.updateInputValue('email', evt)}
									type="text" 
									name="dawnEmail" 
									value={this.state.email}
								/>
							</div>
							<div>
							<div>Password:</div>
								<input 
									onChange={evt => this.updateInputValue('password', evt)}
									type="password" 
									name="dawnPassword" 
									value={this.state.password} 
								/>
							</div>
							<div>
								<input type="submit" onClick={(e)=>{
									e.preventDefault();
									this.props.onClick(this.state.email, this.state.password);
									
									return false;
								}} value="Login" />
							</div>
						</form>
					</div>
				</div>
			)
		}
	}

Login.propTypes = {
	onClick: PropTypes.func.isRequired
}
  
export default Login