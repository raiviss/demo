import React from 'react';
import SquareInfo from '../../squareInfo/container';
import Encounter from '../../encounter/container';
import NpcsAround from '../../npcsAround/container';
import PlayersAround from '../../playersAround/container';
import css from './style.css';

const component = () => (
	<div className="currentPosition">
	  <div><SquareInfo /></div>
	  <div className="otherEnititiesAround">
		  	<div>
			  	<h3>NPCs</h3>
				<div><NpcsAround /></div>
			</div>
			<div>
				<h3>Players</h3>
				<div><PlayersAround /></div>
			</div>
	  </div>
	  <div><Encounter /></div>
	</div>
)

  export default component;