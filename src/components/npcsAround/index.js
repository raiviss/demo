import React from 'react';
import style from './style.css';
import settings from '../../settings';

var key = 0;

const renderAvatar = (entry, clickHandler) => {

	if(typeof entry.userID === 'undefined') {
		return null;
	} else {
		return (<img className="entityAttack" 
			onClick={evt => clickHandler(entry.id) }
			src={settings.NODE_BACKEND_HOST + '/images/avatars/'+entry.userID+'.png'}  alt="attack" />
		);
	}
	
//	return ();
}

const component = ({data, clickHandlerScout, clickHandlerAttack}) => (
	<div className="enitiesAround">
	  <div></div>
	  {data.map((entry)=> {
		  return (<div key={key++} className="otherentities">
					<div className="nameWrap"><img 
						className="direction"
						style={{transform: 'rotate('+entry.AngleFromPlayer+'deg)'}} 
						src={entry.AngleFromPlayer !== -1 ? "/images/arrow.png" : "/images/point.png"}  alt="direction" />
						{renderAvatar(entry, clickHandlerScout)}
						<div className="entityName" onClick={evt => clickHandlerScout(entry) }>{entry.Name}</div>
					</div>
				<img className="entityAttack" onClick={evt => clickHandlerScout(entry) } src="/images/mapicons/hostiles.png"  alt="attack" />
			  </div>
			);
	  })}
	</div>
  )

  export default component;