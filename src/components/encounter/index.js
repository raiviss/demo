import React from 'react';

const component = ({text, options, clickHandler}) => (
	<div>
	  <div>{text}</div>
	  {options.map((option)=> {
		  return (<div onClick={evt => clickHandler(option.id) }>{option.text}</div>);
	  })}
	</div>
  )

  export default component;