import React from 'react'
import PropTypes from 'prop-types'
import css from './style.css';
import settings from '../../settings';

class Chat extends React.Component {
	constructor(props) {
	  super(props);
	  this.state = {
		message: '',
	  };
	}

	updateInputValue(field, evt) {

		this.setState({[field]: evt.target.value});
		
		evt.preventDefault();
	}

	handleSend() {
		
		this.props.sendMessage(this.state.message, this.props.activeChat);
		this.setState({message: ''});
	}

	toHHMM(unix_timestamp) {

		// Create a new JavaScript Date object based on the timestamp
		// multiplied by 1000 so that the argument is in milliseconds, not seconds.
		var date = new Date(unix_timestamp * 1000);
		// Hours part from the timestamp
		var hours = date.getHours();
		// Minutes part from the timestamp
		var minutes = "0" + date.getMinutes();
		// Seconds part from the timestamp
		var seconds = "0" + date.getSeconds();

		return hours + ":" + minutes;
	}

	isActive(chat) {

		return chat === this.props.activeChat;
	}
	
	
	renderPrivateChats() {

		return this.props.privateChats.map((chat)=>{

			return (<div onClick={evt => switchActivehandler(chat.name) }><img src={this.isActive(chat.name) ? "/images/chat/button_active.png" : "/images/chat/button.png"} alt={chat.name} />{chat.name}</div>)
		})
	}

	render () {

		const { messages, switchActivehandler, gangChatEnabled } = this.props;

		if(!gangChatEnabled) {

			var gangChatIcon = "/images/chat/button_disabled.png";

		}else if(this.isActive("GANG")) {
			var gangChatIcon = "/images/chat/button_active.png";
		} else {
			var gangChatIcon = "/images/chat/button.png";
		}

		return (<div className="chatWrap pointerEvents">
			<div className="menuButtons">
				<div onClick={evt => switchActivehandler('GLOBAL') }><img src={this.isActive("GLOBAL") ? "/images/chat/button_active.png" : "/images/chat/button.png"} alt="global" />GLOBAL</div> 
				<div onClick={evt => { if(gangChatEnabled) {switchActivehandler('GANG')} }}><img src={gangChatIcon} alt="global" />GANG</div>  
				<div onClick={evt => switchActivehandler('SECTOR') }><img src={this.isActive("SECTOR") ? "/images/chat/button_active.png" : "/images/chat/button.png"} alt="global" />SECTOR</div>
				{this.renderPrivateChats()}
			</div>
			<div>{messages.map((message)=>
				<div className="messageWrap">
					<img src={settings.NODE_BACKEND_HOST + '/images/avatars/'+message.userid+'.png'} />

					<div className="text">({message.username}) ({this.toHHMM(message.time)}) {message.text}</div>
				</div>
			)}</div>
			<div className="chatInput"><input 
				type="text" 
				value={this.state.message}
				onChange={evt => this.updateInputValue('message', evt) } />
				<button onClick={evt => this.handleSend() }>SEND</button>
			</div>
		</div>);
	}
}

Chat.propTypes = {
	sendMessage: PropTypes.func.isRequired,
	activeChat: PropTypes.string.isRequired
}
  
export default Chat