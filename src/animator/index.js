import fps from 'fpsmeter';

var animator = {};
animator.anims = [];
animator.addAnimation = function(funcAnimaFun){
	var id = Math.random().toString(36).substring(7);
	this.anims.push({id: id,f:funcAnimaFun});
	return id;
}
animator.fps = new FPSMeter();
animator.removeAnimation = function(id){
	for(var i = 0, len = this.anims.length; i < len; i++){
		if(this.anims[i] !== null && this.anims[i].id === id){
			this.anims[i] = null;
			break;
		}
	};
};
animator.animate = function(){
	var self = this;
	
	var fps = 60;
	var count = 60;
	
	requestAnimationFrame( animate );
	
	function animate(){
		//WIP: add 30 fps check here
		for(var i = 0, len = self.anims.length; i < len; i++){
			var anim = self.anims[i];
			if(anim !== null){
				anim.f(anim.id, fps/self.fps.fps); //animation id, animation movement modifier depening on fps
			}
		}
		
		
		count += 0.1;
		//FILTER TEST
	//	map.titles.displacementFilter.offset.x = count * 10//blurAmount * 40;
	//	map.titles.displacementFilter.offset.y = count * 10
//	map.layers.Global.filters = [map.titles.displacementFilter, map.titles.blurFilter1]; //map.titles.displacementFilter
	//FILTER TEST
	
	//	tick += 0.1;
	//	map.layers.Global.position.x-=0.5;
	//	map.layers.Global.position.y-=0.5;
	//	map.layers.Global.rotation = parseInt(45)*(Math.PI/180)
	//	map.renderer.render(map.stage);
		self.fps.tick(); //FPS
		requestAnimationFrame( animate );
	}
}

export default animator;