import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { gameStats } from './gameStats/reducer';
import { map } from './map/reducer';
import { viewpos } from './map/viewpos/reducer';
import { mapUnits } from './map/mapUnits/reducer';
import { path } from './map/path/reducer';
import { user } from './user/reducer';
import { encounter } from './encounter/reducer';
import { npcsAround } from './npcsAround/reducer';
import { mapEncounters } from './map/mapEncounters/reducer';
import { targetDetails } from './targetDetails/reducer';
import { player } from './player/reducer';

const createRootReducer = (history) => combineReducers({
    gameStats,
    map,
    viewpos,
    mapUnits,
    path,
    user,
    mapEncounters,
    encounter,
    npcsAround,
    targetDetails,
    player,
    router: connectRouter(history),
  });

export default createRootReducer;