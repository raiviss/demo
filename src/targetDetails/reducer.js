import { GENERAL_UPDATE } from '../comunicator/actions';
import { CHANGE_TG_STATE, SELECT_NPC_TARGET, SELECT_PLAYER_TARGET } from './actions';

const initialState = {
	stage: 'scout', //scout / fight / 
	Attack: 0,
	Defense: 0,
	EncounterID: 0,
	GlobalPosX: 0,
	GlobalPosY: 0,
	Id: 0,
	Name: '',
	Position: '',
	Position2: '',
	type: '', //npc_boss, npc, player
	fleshType: '', //human / robot / mutant / animal
	details: [], //skills and shit
	scoutIntelHtml: ''
}

export const targetDetails = (state = initialState, action) => {
    switch (action.type) {
		case CHANGE_TG_STATE:

			var newState = {stage: action.data};

			return Object.assign({}, state, newState); 
		break;
		case SELECT_PLAYER_TARGET:
			
			var newState = action.data;
			newState.lastUpdate = Date.now();
			newState.type = 'player';
			newState.stage = 'scout';
			newState.Id = action.data.id;
			return Object.assign({}, state, newState);
		break;
		case SELECT_NPC_TARGET:
			
			var newState = action.data;
			newState.lastUpdate = Date.now();
			newState.type = 'npc';
			newState.stage = 'scout';
			return Object.assign({}, state, newState);
		break;
	  	case GENERAL_UPDATE:

	  		if(typeof action.data.targetDetails !== 'undefined') {
				var newState = action.data.targetDetails;
				
				newState.lastUpdate = Date.now();
				return Object.assign({}, state, newState);
			} else {
				return state;
			}
			
		break;
      default:
        return state
    }
}