import { connect } from 'react-redux';
import NpcsAround from '../components/targetDetails/index';
import { getCurrentSquare } from '../map/reducer';
import { getSocket } from '../comunicator/socket';
import { CHANGE_TG_STATE } from '../targetDetails/actions';
import { action } from '../actions';

const mapStateToProps = (state, ownProps) => {

	const currentSquare = getCurrentSquare(state);

  return {
	playerData: state.user.data,
	stage: state.targetDetails.stage, //scout / fight / result
    Attack: state.targetDetails.Attack,
	Defense: state.targetDetails.Defense,
	fightData: state.targetDetails.fightData,
	EncounterID: state.targetDetails.EncounterID,
	Id: state.targetDetails.Id,
	type: state.targetDetails.type,
	distance: currentSquare.rangeDisplay,
	typeDisplay: currentSquare.typeDisplay,
	Name: state.targetDetails.Name,
	Position: state.targetDetails.Position,
	Position2: state.targetDetails.Position2,
	scoutIntelHtml: state.targetDetails.scoutIntelHtml
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    attackHandler: (id, type, range, tactics) => {

		switch(type) {
			case 'npc':
				action(CHANGE_TG_STATE, 'fight');
				getSocket().emit('apiMessage', {type: 'PUT', method: '/api/combat/attackNPCMap', data: {id, range}});
			break;
			case 'player':
				action(CHANGE_TG_STATE, 'fight');
				getSocket().emit('apiMessage', {type: 'PUT', method: '/api/combat/attackPlayerMap', data: {attack: id, priority: range, tactics, combat: true}});
			break;
			case 'npcboss':

				getSocket().emit('apiMessage', {type: 'PUT', method: '/api/combat/attackBossMap', data: {id, range}});
			break;
		}
	}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NpcsAround)