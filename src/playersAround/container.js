import { connect } from 'react-redux';
import NpcsAround from '../components/npcsAround/index';
import React from 'react';
import { push } from 'connected-react-router';
import { action } from '../actions';
import { SELECT_PLAYER_TARGET } from '../targetDetails/actions';
import { getSocket } from '../comunicator/socket';

const transformData = (mapUnits, playerPos) => {
	//globalPosition: (2) [128, 72]
	//position: (2) [Array(2), Array(2)]
	//userID: "23027"

	playerPos;

	const result = [];

	for(var i = 0, len = mapUnits.length; i < len; i++) {

		const mapUnit = mapUnits[i];

		if(
			Math.abs(mapUnit.globalPosition[0] - playerPos[0]) <=1 &&
			Math.abs(mapUnit.globalPosition[0] - playerPos[0]) <=1
		) {
			
			if((mapUnit.globalPosition[0] - playerPos[0]) === 0 &&
			(mapUnit.globalPosition[0] - playerPos[0]) === 0) {
				var angle = -1;
			} else {
				var angle = ((Math.atan2(
					(mapUnit.globalPosition[0] - playerPos[0]),
					(mapUnit.globalPosition[1] - playerPos[1])
				) * (Math.PI/180)) + 45);
			}

			result.push({
				Name: 'some player',
				userID: mapUnit.userID,
				id: mapUnit.userID,
				AngleFromPlayer: angle
			});
		}
	}

	return result;
}

const playersAround = ({mapUnits, playerPos, clickHandlerScout }) => (
	<NpcsAround clickHandlerScout={clickHandlerScout} data={transformData(mapUnits, playerPos)} />
)


const mapStateToProps = (state, ownProps) => {

  return {
    mapUnits: state.mapUnits.unitsData, 
    playerPos: state.mapUnits.playerPosGlobal, 
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    clickHandlerScout: (data) => {
		//
		dispatch(push('/game/targetplayer/'));
		dispatch(action(SELECT_PLAYER_TARGET, data));
		getSocket().emit('apiMessage', {type: 'PUT', method: '/api/player-scout/scout', data: {Id: data.userID}});
	  }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(playersAround)