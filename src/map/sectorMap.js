import customConnect from '../customConnect';
import sectorBackground from './sectorBackground/index';
import { squareClick } from './sectorBackground/actions';


const changeFunc = ({sectorsData, onClick, positionalOffset}) => {

	sectorBackground(sectorsData, onClick, positionalOffset);
}

const mapStateToProps = (state) => {

	return {
	  sectorsData: state.map.sectorsData,
	  lastUpdate: state.map.lastUpdate,
	  positionalOffset: state.map.positionalOffset
	};
  };

const mapDispatchToProps = (dispatch) => {

	return {
	  onRowClick: data => {
		
	  },
	  onClick: data => {
		
		//in future, add check whats on square
		dispatch(squareClick({pos: data.Pos, pos2: data.SMap}));
	  }
	};
  };


export default customConnect(mapStateToProps, mapDispatchToProps, changeFunc);