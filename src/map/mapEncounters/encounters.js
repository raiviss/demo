import settings from '../../settings';
import unit from '../mapUnits/unit';

export default class encounters extends unit {

	generateSprites(x, y, data) {

		//npc icon
		if(data.Type == 0) {
			var enc = new PIXI.projection.Sprite2d(PIXI.Texture.from('/images/mapicons/hostiles.png'));
		
		} else {
			//enc icon
			var enc = new PIXI.projection.Sprite2d(PIXI.Texture.from('/images/mapicons/enc.png'));
			
		}

		enc.zIndex = 300;
		enc.proj.affine = PIXI.projection.AFFINE.AXIS_X;
		enc.scale.set(0.35, 0.35); 
		enc.position.set(0, 0);
		enc.rotation = -45 * (Math.PI/180);
		
		enc.position.set(x, y+4);

		this.stageUnits.addChild(enc);
	}

	updateEncounters(unitsData, positionalOffset, currPos) {

		this.stageUnits.children = [];

		unitsData.map((entry)=>{

			const position = entry.Position.split('.');
			const position2 = entry.Position2.split('.');
			
			const x = ((position2[1]-currPos[1]) * settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE) + (positionalOffset[1]*settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE) + settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE*settings.SECTOR_WIDTH_AROUND + position[1]*settings.SQUARE_SIZE-37;
			const y = ((position2[0]-currPos[0]) * settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE) + (positionalOffset[0]*settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE) + settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE*settings.SECTOR_WIDTH_AROUND + position[0]*settings.SQUARE_SIZE-47;

			this.generateSprites(x, y, entry);
			
		});

		
		

		if(!this.added) {
			this.rootContainer.addChild(this.stageUnits);
			this.added = true;

			
			
		}
	}
}