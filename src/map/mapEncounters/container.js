import { mainMapContainer } from '../pixiAssets';
import { getPixiApp } from '../pixi';
import encounterClass from './encounters';
import animator from '../../animator/index';
import customConnect from '../../customConnect';

var encounters = new encounterClass({
	rootContainer: mainMapContainer,
	animator: animator,
	getPixiApp: getPixiApp
});

const changeFunc = ({data, positionalOffset, currPos}) => {

	
	encounters.updateEncounters(data, positionalOffset, currPos);
}

const mapStateToProps = (state) => {

	return {
		data: state.mapEncounters.data,
		currPos: state.mapUnits.playerPos[1],
		positionalOffset: state.map.positionalOffset,
		lastUpdate: state.mapEncounters.lastUpdate
	};
  };

const mapDispatchToProps = (dispatch) => {

	return {

	  onClick: data => {
		
		
	  }
	};
  };


export default customConnect(mapStateToProps, mapDispatchToProps, changeFunc);