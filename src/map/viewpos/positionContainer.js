import animator from '../../animator';
import { mainMapContainer } from '../pixiAssets';
import customConnect from '../../customConnect';

const changeFunc = ({scale, viewPos}) => {
	return;
	mainMapContainer.scale.x = scale;
	mainMapContainer.scale.y = scale/2;

	mainMapContainer.position.x = viewPos[0];
	mainMapContainer.position.y = viewPos[1];
}

const mapStateToProps = (state) => {

	return {
	  viewPos: state.viewpos.pos,
	  scale: state.viewpos.scale
	};
  };

const mapDispatchToProps = (dispatch) => {

	return {
	  onRowClick: data => {
		
	  },
	  onClick: data => {
		
	
	  }
	};
  };


export default customConnect(mapStateToProps, mapDispatchToProps, changeFunc);
/*
module.exports = function(renderer){
	//images/smallmaps/map_12.8.jpg

	
	animator.addAnimation(function(){
	//	console.log('test');
	//	 bunny.rotation += 0.01;

		renderer.render(mapView.stage);
	});
}*/