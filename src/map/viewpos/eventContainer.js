import customConnect from '../../customConnect';
import { action } from '../../actions';
import { MAP_VIEW_MOVE, MAP_VIEW_ZOOM} from './actions';

var mouseHold = false;

var initial = [0, 0];
var storePos = [0, 0];
var panHandlerFunc = null;
var zoomHandlerFunc = null;

var mousedownEv = function(ev) {
	mouseHold = true;
	//var storePos = store.mapView.getPos();
	
	initial = [ev.offsetX - storePos[0], ev.offsetY - storePos[1]];
	console.log('mousedownEv', ev);
}
var mouseupEv = function(ev) {

	mouseHold = false;
	console.log('mouseupEv', ev);
}
var mouseMove = function(ev) {

	if(panHandlerFunc === null) {
		return;
	}

	if(mouseHold){
		panHandlerFunc({x: (initial[0]-ev.offsetX)*-1, y:(initial[1]-ev.offsetY)*-1});
	//	console.log('ev', ev);
	}
}

var mouseWheel = function(ev) {

	if(zoomHandlerFunc === null) {
		return;
	}


	if(ev.deltaY > 0){
		zoomHandlerFunc(-0.2, {x: ev.clientX, y: ev.clientY});
	} else {
		zoomHandlerFunc(0.2, {x: ev.clientX, y: ev.clientY});
	}
//	action('MAP_VIEW_MOVE', {x: (initial[0]-ev.offsetX)*-1, y:(initial[1]-ev.offsetY)*-1});
}

var touchdownEv = function(ev) {
	mouseHold = true;
	//var storePos = store.mapView.getPos();
	
	initial = [ev.touches[0].clientX - storePos[0], ev.touches[0].clientY - storePos[1]];
	console.log('mousedownEv', ev);
}
var touchupEv = function(ev) {

	mouseHold = false;
	console.log('mouseupEv', ev);
}
var touchMove = function(ev) {

	if(panHandlerFunc === null) {
		return;
	}

	if(mouseHold){
		panHandlerFunc({x: (initial[0]-ev.touches[0].clientX)*-1, y:(initial[1]-ev.touches[0].clientY)*-1});
	//	console.log('ev', ev);
	}
}

export const ini = function(data){

	return;

	var canvasEl = document.getElementsByTagName("canvas")[0];
	
	if(window.addEventListener) {
		
		canvasEl.addEventListener("mousedown", mousedownEv);
		canvasEl.addEventListener("mouseup", mouseupEv);
		canvasEl.addEventListener("mousemove", mouseMove);
		canvasEl.addEventListener("wheel", mouseWheel);

		canvasEl.addEventListener("touchstart", touchdownEv);
		canvasEl.addEventListener("touchmove", touchMove);
		canvasEl.addEventListener("touchend", touchupEv);

	} else if(window.attachEvent) {
		//wip
	}
	//scroll
}

const changeFunc = ({panHandler, zoomHandler, pos}) => {

	panHandlerFunc = panHandler;
	zoomHandlerFunc = zoomHandler;

	storePos = pos;
}

const mapStateToProps = (state) => {

	return {
	  pos: state.viewpos.pos,
	};
  };

const mapDispatchToProps = (dispatch) => {

	return {
		panHandler: (data) => {

			dispatch(action(MAP_VIEW_MOVE, [data.x, data.y]));
		},
		zoomHandler: (data, pos) => {

			dispatch(action(MAP_VIEW_ZOOM, {change: data, mousePos: [pos.x, pos.y]}));
		}
	};
  };


export default customConnect(mapStateToProps, mapDispatchToProps, changeFunc);