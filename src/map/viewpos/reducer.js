import { GENERAL_UPDATE } from '../../comunicator/actions';
import { MAP_VIEW_MOVE, MAP_VIEW_ZOOM} from './actions';
import settings from '../../settings';

const ZOOM_MOD = 0.5;

const initialState = {
	pos: [0 ,0],
	scale: 1,
//	lastUpdate = Date.now()
}

export const viewpos = (state = initialState, action) => {
    switch (action.type) {
		case MAP_VIEW_MOVE:
			
			return Object.assign(state, {pos: [action.data[0], action.data[1]]});
		break;
		case MAP_VIEW_ZOOM:

			var mod = 1 + (action.data.change*ZOOM_MOD);

			var newScale = state.scale * mod;

			if(newScale > 1.4 || newScale < 0.3) return state;

			state.pos;
			var mousePos = action.data.mousePos;

			var mousePosMap = [
				(mousePos[0] - state.pos[0]), 
				(mousePos[1] - state.pos[1])
			];

			console.log('state.pos', state.pos);
			console.log('mousePosMap', mousePosMap);

			var width = 14 * settings.SQUARE_SIZE;
			var height = 14 * settings.SQUARE_SIZE;

		//	var x = state.pos[0] - (mousePosMap[0] * newScale - mousePosMap[0])/2; state.pos[0] - (mousePosMap[0] ) ; //+ ((settings.SQUARE_SIZE*newScale)/2)
		//	var y = state.pos[1] - (mousePosMap[1] * newScale - mousePosMap[1])/2; //+ ((settings.SQUARE_SIZE*newScale)/2)

			return Object.assign(state, {
				pos: [state.pos[0]+(state.pos[0]*newScale - state.pos[0]), state.pos[1]+(state.pos[1]*newScale - state.pos[1])],
				scale: newScale
			});
		break;
	  case GENERAL_UPDATE:

	  		if(typeof action.data.map !== 'undefined') {
				var newState = action.data.map;
				newState.lastUpdate = Date.now();
				return Object.assign(state, newState);
			} else {
				return state;
			}
			
		break;
      default:
        return state
    }
}