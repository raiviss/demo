import {ini as iniViewpos} from './viewpos/eventContainer';
import { mainMapContainer } from './pixiAssets';
import animator from '../animator/index';

//import { Viewport } from 'pixi-viewport'

var app = null;

export const getPixiApp = () => {
	
	return app;
}

export const ini = function() {

	app = new PIXI.Application({
		width: window.innerWidth, height: window.innerHeight, backgroundColor: 0x404040, //resolution: window.devicePixelRatio || 1,
	});

	document.body.appendChild(app.view);

	// create viewport
	const viewport = new Viewport.Viewport({
		screenWidth: window.innerWidth,
		screenHeight: window.innerHeight,
		worldWidth: 1000,
		worldHeight: 1000,

		interaction: app.renderer.plugins.interaction // the interaction module is important for wheel to work properly when renderer.view is placed or scaled
	})

	// add the viewport to the stage
	// app.stage.addChild(mainMapContainer)
	app.stage.addChild(viewport)

	viewport
    .drag()
    .pinch()
	.wheel()
//	.decelerate()

	var stage = app.stage;
	var renderer = app.renderer;

	

  	 const sprite = viewport.addChild(mainMapContainer);

   	//app.stage.addChild(mainMapContainer);

 
	iniViewpos();

	animator.animate();
	animator.addAnimation(() => {
		// each frame we spin the bunny around a bit
	  // bunny.rotation += 0.01;
	  renderer.render(stage);
   });
   
}

