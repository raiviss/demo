import { GENERAL_UPDATE } from '../comunicator/actions';

const initialState = {
	currentPos: [],
	lastUpdate: Date.now(),
	/*
	* Sector center positional offset.
	* When new sectors are loaded to left/right/down/up, record center possition offset, 
	* used to draw features correctly on the map, as center sector is considered player sector (but it 'shifts' when new stuff is loaded)
	*/
	positionalOffset: [0, 0], 
	sectorsData: [
		
	]
}

export const getCurrentSquare = (state) => {

	if(typeof state.mapUnits.playerPos[0] !== 'undefined' && state.map.sectorsData.length > 0) {

		var yx = state.mapUnits.playerPos[0];
		//var coords = state.map.currentPos[1] + '.' + yx[0] + '.' + yx[1];
		var middleSectorIndx = Math.ceil((state.map.sectorsData.length-1) / 2);
		return state.map.sectorsData[middleSectorIndx][middleSectorIndx][yx[0]-1][yx[1]-1];
	
	} else {
		return {};
	}
}

const offsetPositionalCenter = (newState, oldState) => {

	if(typeof newState.sectorsData === 'undefined') {
		return newState;
	}

	var sectorsData = newState.sectorsData;

	var playerSectorIdx = (sectorsData.length-1)/2;
	
	var currentPlayerSector = sectorsData[playerSectorIdx][playerSectorIdx];

	if(oldState.sectorsData.length > 0) {
		var previousPlayerSector = oldState.sectorsData[playerSectorIdx][playerSectorIdx];
	}

	if(typeof previousPlayerSector !== 'undefined' && currentPlayerSector[0][0].SMap !== previousPlayerSector[0][0].SMap){
		
		var prevYXsec = previousPlayerSector[0][0].SMap.split(".");
		var currYXsec = currentPlayerSector[0][0].SMap.split(".");

		var yDiff = currYXsec[0] - prevYXsec[0];
		var xDiff = currYXsec[1] - prevYXsec[1];
		

		newState.positionalOffset = [
			oldState.positionalOffset[0] + yDiff, 
			oldState.positionalOffset[1] + xDiff
		];
	}

	return newState;
}

export const map = (state = initialState, action) => {
    switch (action.type) {
	  case GENERAL_UPDATE:

	  		if(typeof action.data.map !== 'undefined') {

				var newState = action.data.map;

				newState = offsetPositionalCenter(newState, state);
				newState.lastUpdate = Date.now();

				return Object.assign({}, state, newState);
			} else {
				return state;
			}
			
		break;
      default:
        return state
    }
}