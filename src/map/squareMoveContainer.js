import customConnect from '../customConnect';
import { getSocket } from '../comunicator/socket';

var oldPos = null;

const changeFunc = ({playerPos}) => {

	if(typeof playerPos === 'undefined') {
		return;
	}

	if(oldPos === null) {
		oldPos = playerPos;
		return;
	}

	if(oldPos[0] !== playerPos[0] || oldPos[1] !== playerPos[1]) {

		getSocket().emit('apiMessage', {type: 'GET', method: '/api/map/getSquareIntel', data: []});
	}

}

const mapStateToProps = (state) => {

	return {
		playerPos: state.mapUnits.playerPos[0],
	};
  };

const mapDispatchToProps = (dispatch) => {

	return {
	};
  };


export default customConnect(mapStateToProps, mapDispatchToProps, changeFunc);