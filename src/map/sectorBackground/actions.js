import { getSocket } from '../../comunicator/socket';

export const SQUARE_CLICK = 'SQUARE_CLICK';
export const squareClick = (data) => {

	getSocket().emit('apiMessage', {type: 'PUT', method: '/api/map-path/setPathTo', data: data});

    return {
        type: SQUARE_CLICK,
        origin: 'front',
        data
    }
}