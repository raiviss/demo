import settings from '../../settings';

/*
	//text example
	const sprite = _viewport.addChild(new PIXI.Text('click', { fill: 0xff0000 }))
    sprite.anchor.set(0.5)
    sprite.rotation = Random.range(-0.1, 0.1)
    sprite.position = data.world
    const fade = ease.add(sprite, { alpha: 0 })
    fade.on('done', () => _viewport.removeChild(sprite))
*/

export const generateCoords = ({position, squareData}) => {

	const sprite = new PIXI.Text(squareData.Pos, { fill: 0xff0000, fontSize: 10 });
	sprite.anchor.set(0.5)
	//sprite.proj.affine = PIXI.projection.AFFINE.AXIS_X;
	//sprite.style = new PIXI.TextStyle({fontSize: 10});
	sprite.position.set(position[0], position[1]);
	sprite.rotation = -45 * (Math.PI/180);

	return sprite;
}

export const generateSquareGrid = ({position, squareData}) =>{

	var graphics = new PIXI.Graphics();

	

	graphics.lineStyle(1, 0x0000FF, 1);
	graphics.drawRect(position[0], position[1], settings.SQUARE_SIZE, settings.SQUARE_SIZE);
	graphics.endFill();
//	graphics.rotation = 45 * (Math.PI/180);
//	graphics.moveTo(30,30);

	return graphics;
}

export const generateSquareGraphics = ({position, squareData, clickCallback}) =>{

	if(squareData.Type == 0) {
	
		var sprite3 = new PIXI.projection.Sprite2d(
			PIXI.Texture.from('/images/squaretypes/'+squareData.Type+'_'+(Math.floor(Math.random() * 2)+1)+'.png')
		);

	} else if(squareData.Type == 1) {


		var sprite3 = new PIXI.projection.Sprite2d(
			PIXI.Texture.from('/images/squaretypes/'+squareData.Type+'_'+(Math.floor(Math.random() * 3)+1)+'.png')
		);

	} else if (
		squareData.Type == 0 ||
		squareData.Type == 2 ||
		squareData.Type == 3 ||
		squareData.Type == 8 ||
		squareData.Type == 9 ||
		squareData.Type == 4 ||
		squareData.Type == 3 ||
		squareData.Type == 10 ||
		squareData.Type == 11 ||
		squareData.Type == 12 ||
		squareData.Type == 13
		) {
		var sprite3 = new PIXI.projection.Sprite2d(PIXI.Texture.from('/images/squaretypes/'+squareData.Type+'.png'));
	//	sprite3.anchor.set(-1, 1);
		
	} else {
		var sprite3 = new PIXI.projection.Sprite2d(PIXI.Texture.from('/images/squaretypes/placeholder.png'));
	}


	sprite3.proj.affine = PIXI.projection.AFFINE.AXIS_X;
		sprite3.scale.set(0.087, 0.075); // make it small but tall!
		sprite3.position.set(position[0]-11, position[1]-5);
		sprite3.rotation = -45 * (Math.PI/180);
		// not-proportional scale can't work without special flag `scaleAfterAffine`
		// fortunately, its `true` by default

	sprite3.interactive = true;
	/* //does not work on down btt
	sprite3.on('tap', (event) => {

		console.log('TAP THAT SHIT');
		//handle event
	});*/
	sprite3.on('pointerup', (event) => {

		console.log('POINTER DOWN SHIT 2', squareData);
		clickCallback(squareData);
		//handle event
	});

	return sprite3;
}

export const generateRadIcon = ({position, squareData}) =>{

	

	if(squareData.Rad > 0
		) {
		var sprite3 = new PIXI.projection.Sprite2d(PIXI.Texture.from('/images/mapicons/radb_'+squareData.Rad+'.png'));
	//	sprite3.anchor.set(-1, 1);
		
	}

	sprite3.proj.affine = PIXI.projection.AFFINE.AXIS_X;
		sprite3.scale.set(0.35, 0.35); // make it small but tall!
		sprite3.position.set(position[0], position[1]);
		sprite3.alpha = 0.5;
		
		sprite3.rotation = -45 * (Math.PI/180);
		// not-proportional scale can't work without special flag `scaleAfterAffine`
		// fortunately, its `true` by default

	return sprite3;
}