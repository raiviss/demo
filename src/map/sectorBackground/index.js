import { generateSquareGrid, generateSquareGraphics, 
	generateRadIcon, generateCoords } from './square';
import { getPixiApp } from '../pixi';
import { mainMapContainer } from '../pixiAssets';
import settings from '../../settings';

var isoScalingContainer = null;

var stageSect = new PIXI.Container();
	stageSect.zIndex = 1;
	stageSect.rotation = 45 * (Math.PI/180);
	stageSect.x = 0;
	stageSect.y = -0;

var existingSectors = [];

const gradient = (from, to, sizeX, sizeY) => {

	const c = document.createElement("canvas");
	const ctx = c.getContext("2d");
	const grd = ctx.createLinearGradient(0,0,100,100);
	grd.addColorStop(0, from);
	grd.addColorStop(1, to);
	ctx.fillStyle = grd;
	ctx.fillRect(0,0,200,500);
	return new PIXI.Texture.from(c);
}

const generateSector = (sector, clickHandler) => {

	
	
	var sectorContainer = new PIXI.Container();

	for(var y = 0, ylen = sector.length; y < ylen; y++) {

		for(var x = 0, xlen = sector.length; x < xlen; x++) {
			var squareData = sector[x][y];

			

		/*	sectorContainer.addChild(generateSquareGrid({
				position: [
					sectorsSizeY + y*settings.SQUARE_SIZE, 
					sectorsSizeX + x*settings.SQUARE_SIZE
				], 
				squareData}));*/

				sectorContainer.addChild(generateSquareGraphics({position: [
				 y*settings.SQUARE_SIZE, 
				 x*settings.SQUARE_SIZE
			], 
				squareData,
				clickCallback:clickHandler
			}));

			if(squareData.Rad > 0) {
				sectorContainer.addChild(generateRadIcon({position: [
					y*settings.SQUARE_SIZE+6, 
					x*settings.SQUARE_SIZE+5
				], squareData}));
			}

		/*	stageSect.addChild(generateCoords({position: [
				sectorsSizeY + y*settings.SQUARE_SIZE+5, 
				sectorsSizeX + x*settings.SQUARE_SIZE+5
			], squareData}));*/
			
		}
	}

	return sectorContainer;
}

export default (sectorsData, clickHandler, positionalOffset) => {

	if(typeof sectorsData[0] === 'undefined') {
		return;
	} 

	var pixiApp = getPixiApp();

	var sectorContainers = [];
	
/*	var stageSquareGraphSect = new PIXI.Container();
	stageSquareGraphSect.rotation = 45 * (Math.PI/180);*/


	//currently 25 (upgraded) or 9 (free) sectors visible at the same time, so player sector index is 2,2, or 1,1 (in the middle)
	/*var playerSectorIdx = (sectorsData.length-1)/2;
	
	var currentPlayerSector = sectorsData[playerSectorIdx][playerSectorIdx];

	if(existingSectors.length > 0) {
		var previousPlayerSector = existingSectors[playerSectorIdx][playerSectorIdx];
	}
	*/

	if(1 === 2 && typeof previousPlayerSector !== 'undefined' && currentPlayerSector[0][0].SMap !== previousPlayerSector[0][0].SMap) {
		
		// Attement to merge new sectors and delete old from existing stageSect.childrens array

	/*	var prevYXsec = previousPlayerSector[0][0].SMap.split(".");
		var currYXsec = currentPlayerSector[0][0].SMap.split(".");

		var allSecWidth = sectorsData.length;

		if(positionalOffset[0] >= 1) {
			
			var startY = allSecWidth - 1;
			
		} else if(positionalOffset[0] <= -1 ) {
			
			
			var startY = 0;
		}

		if(positionalOffset[1] >= 1) {
			var startX = allSecWidth - 1;
		} else if (positionalOffset[1] <= -1 ) {
			var startX = 0;
		}

		var sectorIndxToRemove = [];
		if(typeof startY !== 'undefined') {
			for(var x = 0, len = sectorsData.length; x < len; x++) {
				var sector = sectorsData[startY][x];
				var squareLen = sector.length;


				const sectorsSizeX = (positionalOffset[1] * squareLen * settings.SQUARE_SIZE) + squareLen * settings.SQUARE_SIZE * x;
				const sectorsSizeY = (positionalOffset[0] * squareLen * settings.SQUARE_SIZE) + squareLen * settings.SQUARE_SIZE * startY+1;

				var sectorContainer = generateSector(sector, clickHandler);
				sectorContainer.y = sectorsSizeY;
				sectorContainer.x = sectorsSizeX;

				sectorContainers.push(sectorContainer);

				if(positionalOffset[0] > 0) {
					sectorIndxToRemove.push(0 + x);
				} else {
					sectorIndxToRemove.push((sectorsData.length*sectorsData.length - sectorsData.length) + x);
				}
				
			}
		}

		for (var i = sectorIndxToRemove.length -1; i >= 0; i--) {
			stageSect.children.splice(sectorIndxToRemove[i],1);
		}*/

		//stageSect.children.splice(6, 1);
	} else {

		stageSect.children = [];

		

		for(var sy = 0, sylen = sectorsData.length; sy < sylen; sy++) {
			for(var sx = 0, sxlen = sectorsData[sy].length; sx < sxlen; sx++) {

				var sector = sectorsData[sy][sx];

				var squareLen = sector.length;
				const sectorsSizeX = (positionalOffset[1] * squareLen * settings.SQUARE_SIZE) +squareLen * settings.SQUARE_SIZE * sx;
				const sectorsSizeY = (positionalOffset[0] * squareLen * settings.SQUARE_SIZE) +squareLen * settings.SQUARE_SIZE * sy;

				const graphics = new PIXI.Graphics(); //sector rectangle
				graphics.lineStyle(4, 0x929292, 1); 
			//	graphics.beginTextureFill(gradient('#391b1b', 'transparent', squareLen * settings.SQUARE_SIZE, squareLen * settings.SQUARE_SIZE))
				
				graphics.drawRect(0, 0, squareLen * settings.SQUARE_SIZE, squareLen * settings.SQUARE_SIZE);
				
				graphics.endFill();
				var texture = getPixiApp().renderer.generateTexture(graphics);
				
				const sectorRectangle = new PIXI.projection.Sprite2d(texture);
				sectorRectangle.x = sectorsSizeX+8;
				sectorRectangle.y = sectorsSizeY-13;
				
				var sectorContainer = generateSector(sector, clickHandler);
				sectorContainer.y = sectorsSizeY;
				sectorContainer.x = sectorsSizeX;
				
				sectorContainers.push(sectorRectangle);
				sectorContainers.push(sectorContainer);
				
			}
		}
	}
	
	existingSectors = sectorsData;

	pixiApp.renderer.render(pixiApp.stage);

	sectorContainers.map((secCont)=>{
		
		stageSect.addChild(secCont);
	})
	

	mainMapContainer.addChild(stageSect);
}