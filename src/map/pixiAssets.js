export var mainMapContainer = new PIXI.Container();
mainMapContainer.scale.y = 0.5; // isometry can be achieved by setting scaleY 0.5 or tan(30 degrees)
mainMapContainer.position.set(window.innerWidth/2, 0);