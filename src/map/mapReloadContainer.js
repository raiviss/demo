import customConnect from '../customConnect';
import { getSocket } from '../comunicator/socket';

var oldPos = null;

const changeFunc = ({playerPos2}) => {

	if(typeof playerPos2 === 'undefined') {
		return;
	}

	if(oldPos === null) {
		oldPos = playerPos2;
		return;
	}

	if(oldPos[0] !== playerPos2[0] || oldPos[1] !== playerPos2[1]) {
		getSocket().emit('apiMessage', {type: 'GET', method: '/api/map/currentSectors', data: []});
		oldPos = playerPos2;
	}

	if(oldPos[0] !== playerPos2[0]) {

		getSocket().emit('apiMessage', {type: 'GET', method: '/api/encounter-stage/getEncounter', data: []});
	}

}

const mapStateToProps = (state) => {

	return {
		playerPos2: state.mapUnits.playerPos[1],
	};
  };

const mapDispatchToProps = (dispatch) => {

	return {
	};
  };


export default customConnect(mapStateToProps, mapDispatchToProps, changeFunc);