import customConnect from '../../customConnect';
import { mainMapContainer } from '../pixiAssets';
import { map } from '../reducer';
import settings from '../../settings';

var pathCont = new PIXI.Container();
pathCont.rotation = 45 * (Math.PI/180);
pathCont.zIndex = 90;

var added = false;

const constDrawPath = (pathData, positionalOffset) => {

	pathCont.children = [];

	if(pathData.length === 0) {
		return;
	}

	const realPath = new PIXI.Graphics();
	const realPathBorder = new PIXI.Graphics();

	realPath.position.set(-3, -25);
	realPathBorder.position.set(-3, -25);

	realPath.lineStyle(6, 0xbcd351, 1);
	realPathBorder.lineStyle(10, 0x736f6a, 1);

	realPath.alpha = 0.4;
	realPathBorder.alpha = 0.8;

	const smap2 =  pathData[0].pos2;

	const iniX = (positionalOffset[1] * settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE) + settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE*settings.SECTOR_WIDTH_AROUND + pathData[0].pos[1]*settings.SQUARE_SIZE;
	const iniY = (positionalOffset[0] * settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE) +settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE*settings.SECTOR_WIDTH_AROUND + pathData[0].pos[0]*settings.SQUARE_SIZE;

	realPath.moveTo(iniX, iniY);
	realPathBorder.moveTo(iniX, iniY);

	//realPath.position.x = pathData[0].pos[1]*settings.SQUARE_SIZE;
	//realPath.position.y = pathData[0].pos[0]*settings.SQUARE_SIZE;

	pathData.map((entry)=>{

		const x = (positionalOffset[1] * settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE) +(entry.pos2[1] - smap2[1] ) * settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE + settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE*settings.SECTOR_WIDTH_AROUND +entry.pos[1]*settings.SQUARE_SIZE;
		const y = (positionalOffset[0] * settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE) +( entry.pos2[0] - smap2[0]) * settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE + settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE*settings.SECTOR_WIDTH_AROUND +entry.pos[0]*settings.SQUARE_SIZE;

		realPath.lineTo(x, y);
		realPathBorder.lineTo(x, y);
	});

	
	
	realPath.zIndex = 100;

	pathCont.addChild(realPathBorder);
	pathCont.addChild(realPath);

	if(!added) {
		mainMapContainer.addChild(pathCont);
		added = true;
	}
}

const changeFunc = ({pathData, positionalOffset}) => {

	constDrawPath(pathData, positionalOffset);
}

const mapStateToProps = (state) => {

	return {
		pathData: state.path.data,
		positionalOffset: state.map.positionalOffset
	};
  };

const mapDispatchToProps = (dispatch) => {

	return {

	  onClick: data => {
		
		
	  }
	};
  };


export default customConnect(mapStateToProps, mapDispatchToProps, changeFunc);