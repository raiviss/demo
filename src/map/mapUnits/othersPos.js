import { mainMapContainer } from '../pixiAssets';
import { getPixiApp } from '../pixi';
import playerUnits from './palyerUnits';

import animator from '../../animator/index';

var stageUnits = new PIXI.Container();
stageUnits.rotation = 45 * (Math.PI/180);
stageUnits.zIndex = 100;
var added = false;

var addedUnits = [];

var pu = new playerUnits({
	rootContainer: mainMapContainer,
	animator: animator,
	getPixiApp: getPixiApp
});

export const renderOthersPos = ({unitsData, positionalOffset}) => {

	//stageUnits.children = [];

	pu.update(unitsData, positionalOffset);

	mainMapContainer.children.sort((itemA, itemB) => itemA.zIndex - itemB.zIndex);
	
}