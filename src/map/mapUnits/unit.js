export default class unit {
	
	constructor({rootContainer, animator, getPixiApp}) {
		
		this.rootContainer = rootContainer;
		this.units = [];

		this.getPixiApp = getPixiApp;
		this.stageUnits = new PIXI.Container();
		this.stageUnits.rotation = 45 * (Math.PI/180);
		this.stageUnits.zIndex = 100;
		this.added = false;
		this.addedUnits = [];

		this.animator = animator;
	}
}
