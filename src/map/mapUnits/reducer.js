import { GENERAL_UPDATE } from '../../comunicator/actions';

const initialState = {
	unitsData: [],
	playerPos: [0, 0],
	playerPosGlobal: [0, 0],
	lastUpdate: Date.now(),
}

export const mapUnits = (state = initialState, action) => {
    switch (action.type) {
	  case GENERAL_UPDATE:

	  		if(typeof action.data.mapUnits !== 'undefined') {
				var newState = action.data.mapUnits;
				newState.lastUpdate = Date.now();
				return Object.assign({}, state, newState);
			} else {
				return state;
			}
			
		break;
      default:
        return state
    }
}