import settings from '../../settings';
import { mainMapContainer } from '../pixiAssets';
import { getPixiApp } from '../pixi';
import animator from '../../animator/index';

var stageUnits = new PIXI.Container();
stageUnits.rotation = 45 * (Math.PI/180);
stageUnits.zIndex = 100;
var added = false;

var playerSprite = null;
var playerAvatar = null;
var avatarMask = null;

export const renderPlayerPos = ({playerPos, positionalOffset}) => {

	//stageUnits.children = [];

	if(typeof playerPos === 'undefined') {
		return;
	}

	const x = (positionalOffset[1]*settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE) + settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE*settings.SECTOR_WIDTH_AROUND + playerPos[1]*settings.SQUARE_SIZE-48;
	const y = (positionalOffset[0]*settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE) + settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE*settings.SECTOR_WIDTH_AROUND + playerPos[0]*settings.SQUARE_SIZE-56;

	if(!added) {
	
		playerSprite = new PIXI.projection.Sprite2d(PIXI.Texture.from('/images/mapicons/player_icon.png'));
		playerAvatar = new PIXI.projection.Sprite2d(PIXI.Texture.from('http://localhost:2000/images/avatars/1.png'));

		playerAvatar.zIndex = 300;
		playerAvatar.proj.affine = PIXI.projection.AFFINE.AXIS_X;
		playerAvatar.scale.set(0.2, 0.2);
		playerAvatar.rotation = -45 * (Math.PI/180);

		playerAvatar.position.set(x, y);

		playerSprite.zIndex = 299;
		playerSprite.proj.affine = PIXI.projection.AFFINE.AXIS_X;
		playerSprite.scale.set(0.2, 0.2);
		playerSprite.rotation = -45 * (Math.PI/180);

		playerSprite.position.set(x, y);

		

		// MASK (clip things outside the background border)
		var px_mask_outter_bounds = new PIXI.Graphics();
		px_mask_outter_bounds.beginFill(0xFFFFFF);
		px_mask_outter_bounds.drawCircle (0, 0, 50); // In this case it is 8000x8000
		
		px_mask_outter_bounds.endFill();
		var texture = getPixiApp().renderer.generateTexture(px_mask_outter_bounds);

		avatarMask = new PIXI.projection.Sprite2d(texture);
		avatarMask.proj.affine = PIXI.projection.AFFINE.AXIS_X;
		avatarMask.rotation = -45 * (Math.PI/180);
		avatarMask.position.set(x, y);
		avatarMask.scale.set(0.2, 0.2);
		

	//	stageUnits.mask = '';

		
		

		var avatarContainer = new PIXI.Container();
		stageUnits.addChild(avatarContainer);

		avatarContainer.addChild(playerAvatar);
		avatarContainer.addChild(avatarMask);
		avatarContainer.mask = avatarMask;
		
		stageUnits.addChild(playerSprite);
	} else {

	/*	animator.addAnimation((animID)=>{

			playerSprite.x += (x - playerSprite.x) * 0.1;
			playerSprite.y += (y - playerSprite.y) * 0.1;

			playerAvatar.x += (x - playerAvatar.x) * 0.1;
			playerAvatar.y += (y - playerAvatar.y) * 0.1;

			avatarMask.x += (x - avatarMask.x) * 0.1;
			avatarMask.y += (y - avatarMask.y) * 0.1;

			if (Math.abs(playerSprite.x - x) < 1 && Math.abs(playerSprite.y - y) < 1) {*/
				playerSprite.x = x;
				playerSprite.y = y;

				playerAvatar.x = x;
				playerAvatar.y = y;

				avatarMask.x = x;
				avatarMask.y = y;
	/*			animator.removeAnimation(animID);
			}
		});*/
	/*	playerSprite.position.set(
			x, 
			y
		);*/
	}
	

	
	
	

	

	if(!added) {
		mainMapContainer.addChild(stageUnits);
		added = true;

		
		mainMapContainer.children.sort((itemA, itemB) => itemA.zIndex - itemB.zIndex);
	}
	
}