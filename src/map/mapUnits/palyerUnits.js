import settings from '../../settings';
import unit from './unit';

export default class playerUnit extends unit {

	
	constructor({rootContainer, animator, getPixiApp}) {

		super({rootContainer, animator, getPixiApp});

		this.rootContainer = rootContainer;
		this.units = [];

		this.getPixiApp = getPixiApp;
		this.stageUnits = new PIXI.Container();
		this.stageUnits.rotation = 45 * (Math.PI/180);
		this.stageUnits.zIndex = 100;
		this.added = false;
		this.addedUnits = [];

		this.animator = animator;
	}
	
	generateSprites(x, y, id) {

		//pin icon
		var sprite3 = new PIXI.projection.Sprite2d(PIXI.Texture.from('/images/mapicons/others_icon.png'));
		sprite3.zIndex = 300;
		sprite3.proj.affine = PIXI.projection.AFFINE.AXIS_X;
		sprite3.scale.set(0.15, 0.15); 
		sprite3.position.set(0, 0);
		sprite3.rotation = -45 * (Math.PI/180);

		//player avatar
		var playerAvatar = new PIXI.projection.Sprite2d(PIXI.Texture.from(settings.NODE_BACKEND_HOST + '/images/avatars/'+id+'.png'));
		playerAvatar.zIndex = 299;
		playerAvatar.proj.affine = PIXI.projection.AFFINE.AXIS_X;
		playerAvatar.scale.set(0.15, 0.15);
		playerAvatar.rotation = -45 * (Math.PI/180);
		playerAvatar.position.set(0, 0);

		//mask to cut round avatar
		var px_mask_outter_bounds = new PIXI.Graphics();
		px_mask_outter_bounds.beginFill(0xFFFFFF);
		px_mask_outter_bounds.drawCircle (0, 0, 50); // In this case it is 8000x8000
		
		px_mask_outter_bounds.endFill();
		var texture = this.getPixiApp().renderer.generateTexture(px_mask_outter_bounds);

		var avatarMask = new PIXI.projection.Sprite2d(texture);
		avatarMask.proj.affine = PIXI.projection.AFFINE.AXIS_X;
		avatarMask.rotation = -45 * (Math.PI/180);
		avatarMask.position.set(0, 0);
		avatarMask.scale.set(0.15, 0.15);

		var allContainer = new PIXI.Container();
		var avatarContainer = new PIXI.Container();
		

		avatarContainer.addChild(playerAvatar);
		avatarContainer.addChild(avatarMask);
		avatarContainer.mask = avatarMask;
		
		allContainer.addChild(avatarContainer);
		allContainer.addChild(sprite3);
		allContainer.position.set(x, y);

		this.stageUnits.addChild(allContainer);

		this.addedUnits[id] = allContainer;
	}

	moveSprites (x, y, id) {

		this.animator.addAnimation((animID) => {

			var spriteContainer = this.addedUnits[id];

			
			spriteContainer.x += (x - spriteContainer.x) * 0.1;
			spriteContainer.y += (y - spriteContainer.y) * 0.1;

			if (Math.abs(spriteContainer.x - x) < 1 && Math.abs(spriteContainer.y - y) < 1) {
				spriteContainer.x = x;
				spriteContainer.y = y;
				this.animator.removeAnimation(animID);
			}
		});
	}

	update(unitsData, positionalOffset) {

		var unitsProcessed = [];

		unitsData.map((entry)=>{

			
			const x = (positionalOffset[1]*settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE) + settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE*settings.SECTOR_WIDTH_AROUND + entry.position[0][1]*settings.SQUARE_SIZE-37;
			const y = (positionalOffset[0]*settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE) +settings.SECTOR_SQUARE_WIDTH*settings.SQUARE_SIZE*settings.SECTOR_WIDTH_AROUND + entry.position[0][0]*settings.SQUARE_SIZE-47;


			if(typeof this.addedUnits[(entry.userID)] !== 'undefined') {

				this.moveSprites(x, y, entry.userID)
				
			} else {

				this.generateSprites(x, y, entry.userID);
			}

			unitsProcessed.push(entry.userID);
			
		});

		
		for (var userID in this.addedUnits) {

			if(unitsProcessed.indexOf(userID) === -1) {

				this.stageUnits.removeChild(this.addedUnits[userID]);
				this.addedUnits[userID] = undefined;
				
			}
		}
		

		if(!this.added) {
			this.rootContainer.addChild(this.stageUnits);
			this.added = true;

			
			
		}
	}
}