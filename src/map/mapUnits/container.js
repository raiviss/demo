import customConnect from '../../customConnect';

import { renderPlayerPos } from './playerPos';
import { renderOthersPos } from './othersPos';



const changeFunc = ({unitsData, playerPos, onClick, positionalOffset}) => {

	renderPlayerPos({playerPos, positionalOffset});
	renderOthersPos({unitsData, positionalOffset})
}

const mapStateToProps = (state) => {

	return {
		unitsData: state.mapUnits.unitsData,
		playerPos: state.mapUnits.playerPos[0],
		positionalOffset: state.map.positionalOffset,
	  	lastUpdate: state.map.lastUpdate
	};
  };

const mapDispatchToProps = (dispatch) => {

	return {

	  onClick: data => {
		
		
	  }
	};
  };


export default customConnect(mapStateToProps, mapDispatchToProps, changeFunc);