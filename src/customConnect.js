import DeepEqual from 'deep-equal';

var isChanged = (refState, newStateProps) => {

  return !DeepEqual(refState, newStateProps);

};

export default (mapStateToPropsRes, mapDispatchToPropsRes, changeFunc) => {

	return (store) => {

		var mapStateObject = mapStateToPropsRes(store.getState());
  
		var refState = mapStateObject; //Object.assign({}, mapStateObject);
		var dispatchProps = mapDispatchToPropsRes(store.dispatch);
		
		changeFunc(Object.assign({}, mapStateObject, dispatchProps));
	
		store.subscribe((state) => {
	
			var newPropChanges = mapStateToPropsRes(store.getState());
			var newDispatchChanges = mapDispatchToPropsRes(store.dispatch);
	
			if(isChanged(refState, newPropChanges)) {
				refState = newPropChanges;
				changeFunc(Object.assign({}, newPropChanges, newDispatchChanges));
			}
		});
	}
	
  };
/*
export default (mapStateToPropsRes, mapDispatchToPropsRes, store, changeFunc) => {

  var mapStateObject = mapStateToPropsRes(store.getState());


  var refState = mapStateObject; //Object.assign({}, mapStateObject);
  var dispatchProps = mapDispatchToPropsRes(store.dispatch);
  
  changeFunc(Object.assign({}, mapStateObject, dispatchProps));

  store.subscribe((state) => {

  	var newPropChanges = mapStateToPropsRes(store.getState());
  	var newDispatchChanges = mapDispatchToPropsRes(store.dispatch);

    if(isChanged(refState, newPropChanges)) {
      refState = newPropChanges;
      changeFunc(Object.assign({}, newPropChanges, newDispatchChanges));
    }
  });
};*/