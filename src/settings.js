const settings = {
	WS_PORT: 3000,
	HTTP_API_PORT: 2000, //rest api listen port (express.js)
	SQUARE_SIZE: 25,
	SECTOR_WIDTH_AROUND: 1, //how much sectors visible around
	SECTOR_SQUARE_WIDTH: 14,
	NODE_BACKEND_HOST: 'http://localhost:2000',
}

export default settings;