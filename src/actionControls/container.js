import { connect } from 'react-redux'
import ActionControls from '../components/actionControls/index'
import { cancelPath } from './actions';

const mapStateToProps = (state, ownProps) => {

  return {
	cancelPathActive: (state.path.data.length > 1)
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    cancelPathHandler: () => {

		dispatch(cancelPath());

    //  
    
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ActionControls)