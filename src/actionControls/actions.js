import { getSocket } from '../comunicator/socket';

export const CANCEL_PATH_CLICK = 'CANCEL_PATH_CLICK';
export const cancelPath = () => {

	getSocket().emit('apiMessage', {type: 'PUT', method: '/api/map-path/cancelPath', data: {}});

    return {
        type: CANCEL_PATH_CLICK,
        origin: 'front',
        data:{}
    }
}