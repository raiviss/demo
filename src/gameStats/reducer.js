import { UPDATE_ONLINE } from './actions';

const initialState = {
    onlineCount: 0
}

export const gameStats = (state = initialState, action) => {
    switch (action.type) {
      case UPDATE_ONLINE: //
        return Object.assign({onlineCount: action.data}, state);
      default:
        return state
    }
  }