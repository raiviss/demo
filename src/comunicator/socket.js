import io from 'socket.io-client';
import settings from '../settings';
import { GENERAL_UPDATE } from './actions';
import { actionBEnd } from '../actions';
import Cookies from 'js-cookie';
import {store} from '../store';
import {store as chatStore} from '../chatApp/store';
import { ADD_MESSAGE } from '../chat/actions';

var socket = null;

export const getSocket = () => {

	return socket;
}

export const startSocket = () => {

	console.log('START SOCKET');
	console.log('sid: ', Cookies.get("express.sid"));

	socket = io(window.location.protocol + '//' + window.location.hostname + ':' + settings.WS_PORT, {
		query: 'express.sid=' + Cookies.get('express.sid')
	});

	socket.on('error', (resp) => {

		console.log('error', resp);

		socket.disconnect();
	//	this.setState(Object.assign(this.state, {auth: false, socket: null, wrongPass: true}));
	});

	// this.setState(Object.assign(this.state, {auth: true, socket: socket, wrongPass: false}));


	socket.emit('apiMessage', {type: 'GET', method: '/api/player/getall', data: {}});


	socket.on('generalUpdate', function(data){
		

		if(typeof data.chat !== 'undefined') {
			chatStore.dispatch(actionBEnd(GENERAL_UPDATE, data));
		}

		store.dispatch(actionBEnd(GENERAL_UPDATE, data));
		
	});

	socket.on('chatMessage', function(data){
		
		chatStore.dispatch(actionBEnd(ADD_MESSAGE, data));
	});
}