import settings from '../settings';

// Example POST method implementation:
export const post = (url = '', data = {}) => {
	// Default options are marked with *
	return fetch(window.location.protocol + '//' + window.location.hostname + ':' + settings.HTTP_API_PORT + '/' + url, {
		method: 'POST',
		credentials: "include",
		headers: {
		  'Accept': 'application/json',
		  'Content-Type': 'application/json',
		  'mode': 'cors',
		},
		body: JSON.stringify(data)}).then((resp)=>{

		return resp.json();
	})
//	return response.json(); // parses JSON response into native JavaScript objects
  }