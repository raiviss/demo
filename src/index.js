import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import App from './components/app';
import { ConnectedRouter } from 'connected-react-router';
import { history, store } from './store';
import { push } from 'connected-react-router';
import Cookies from 'js-cookie';
import sectorMap from './map/sectorMap';
import viewposMap from './map/viewpos/eventContainer';
import mapUnits from './map/mapUnits/container';
import mapBackground from './map/viewpos/positionContainer';
import path from './map/path/container';
import mapEncounters from './map/mapEncounters/container';
import mapReloadContainer from './map/mapReloadContainer';
import squareMoveContainer from './map/squareMoveContainer';

//if cookie (or token in future) present and not expired, redirect to game
console.log('sid cookie', Cookies.get("express.sid"));
if(typeof Cookies.get("express.sid") !== "undefined" && window.location.pathname.indexOf('/game') === -1) {
  //store.dispatch(push('/game'));
  window.location.href = "/game";
} else {
  render(
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <App push={(url)=> store.dispatch(push(url))} />
      </ConnectedRouter>
    </Provider>,
    document.getElementById('app')
  );
}

sectorMap(store);
viewposMap(store);
mapBackground(store);
path(store);
mapUnits(store);
mapEncounters(store);
mapReloadContainer(store);
squareMoveContainer(store);