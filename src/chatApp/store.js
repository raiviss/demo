import { combineReducers, createStore } from 'redux';
import { chat } from '../chat/reducer';

const mainReducer = combineReducers({
	chat
});

export const store = createStore(mainReducer)