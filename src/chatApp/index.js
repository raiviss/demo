import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import ChatComp from '../chat/container';
import { store } from './store';
import { chatContext } from './context';


export default class chatApp extends Component {

  constructor(props) {
    super(props)
    this.store = store
  }

  render() {
    return (
      <Provider store={this.store} context={chatContext}>
        <ChatComp />
      </Provider>
    )
  }
}