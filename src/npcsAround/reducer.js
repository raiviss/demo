import { GENERAL_UPDATE } from '../comunicator/actions';

const initialState = {
	data: []
}

export const npcsAround = (state = initialState, action) => {
    switch (action.type) {
	  case GENERAL_UPDATE:

	  		if(typeof action.data.npcsAround !== 'undefined') {
				var newState = action.data.npcsAround;
				newState.lastUpdate = Date.now();
				return Object.assign({}, state, newState);
			} else {
				return state;
			}
			
		break;
      default:
        return state
    }
}