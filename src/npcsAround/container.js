import { connect } from 'react-redux';
import NpcsAround from '../components/npcsAround/index';
import { action } from '../actions';
import {SELECT_NPC_TARGET} from '../targetDetails/actions';
import { push } from 'connected-react-router';

const mapStateToProps = (state, ownProps) => {



  return {
    data: state.npcsAround.data, 
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    clickHandlerScout: (npcData) => {
      //
      dispatch(push('/game/targetnpc/'));
      dispatch(action(SELECT_NPC_TARGET, npcData));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NpcsAround)