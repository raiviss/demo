import { GENERAL_UPDATE } from '../comunicator/actions';

const initialState = {
	
}

/*
	'userID'=>$upData['Userid'],
	'factID'=>$upData['Pfacid'],
	'groupSkills'=>$this->getGroupSkills(),
	'charName'=>'CHName',
	'Vehicle'=>$upData['Vehicle'],
	'stamina'=>$this->currentStamina(),
	'health'=>$this->currentHealth(),
	'levelData'=>$this->getPlayerLevel(),
	'maxstamina'=>$maxstamina
*/

export const player = (state = initialState, action) => {
    switch (action.type) {
	  case GENERAL_UPDATE:

	  		if(typeof action.data.player !== 'undefined') {
				var newState = action.data.player;
				newState.lastUpdate = Date.now();
				return Object.assign({}, state, newState);
			} else {
				return state;
			}
			
		break;
      default:
        return state
    }
}