// action generator class

export const action = (constDef, data) => {

    return {
        type: constDef,
        origin: 'front',
        data
      }
}

//action backend
export const actionBEnd = (constDef, data) => {

  return {
    type: constDef,
    origin: 'back',
    data
  }
}