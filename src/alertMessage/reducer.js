import { GENERAL_UPDATE } from '../comunicator/actions';

const initialState = {
	gameMessages: [], //game specific alert messages, about server restart, etc.
	successMessages: [], //mission completed, item crafted and so on
	warnMessages: [], //not enoguh crafting materials, no fuel, etc
	errorMessages: [], //error messages
}

export const alertMessage = (state = initialState, action) => {
    switch (action.type) {
	  case GENERAL_UPDATE:

	  		if(typeof action.data.alertMessage !== 'undefined') {
				var newState = action.data.alertMessage;

				//To DO: check for each message

				newState.lastUpdate = Date.now();
				return Object.assign({}, state, newState);
			} else {
				return state;
			}
			
		break;
      default:
        return state
    }
}