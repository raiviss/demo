import { connect } from 'react-redux'
import { setVisibilityFilter } from '../actions'
import Login from '../components/user/login/index'
import { startSocket } from '../comunicator/socket';
import { post } from '../comunicator/http';
import { push } from 'connected-react-router';

const mapStateToProps = (state, ownProps) => {
  return {
    active: ownProps.filter === state.visibilityFilter
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onClick: (user, password) => {

      post('login', {username: user, password}).then((resp)=>{

        if(resp && resp.result) {
        
          dispatch(push('/game'));
          
        } else {

        }

      })

    //  
    
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)