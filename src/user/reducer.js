import { GENERAL_UPDATE } from '../comunicator/actions';

const initialState = {
	userID: 0,
	gangID: 0,
	characterName: '',
	email: '',
	xp: 0,
	lvl: 0
}

export const user = (state = initialState, action) => {
    switch (action.type) {
	  case GENERAL_UPDATE:

	  		if(typeof action.data.user !== 'undefined') {
				var newState = action.data.user;
				newState.lastUpdate = Date.now();
				return Object.assign({}, state, newState);
			} else {
				return state;
			}
			
		break;
      default:
        return state
    }
}