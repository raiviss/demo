import React from 'react';
import GamePanel from '../components/gamePanel/index';
import { startSocket } from '../comunicator/socket';
import { ini } from '../map/pixi';
import { connect } from 'react-redux'

class gamePanelCoponent extends React.Component {
	

	componentDidMount(field, evt) {

		startSocket();
		ini();
	}
	
	render () {

		return (<GamePanel push={this.props.push} />);
	}
}

const mapStateToProps = (state, ownProps) => {

	return {
		push: null
	}
  }
  
  const mapDispatchToProps = (dispatch, ownProps) => {
	return {
	  cancelPathHandler: () => {
  
	//	  dispatch(cancelPath());
  
	  //  
	  
	  }
	}
  }
  
  //export default connect(mapStateToProps, mapDispatchToProps)(gamePanelCoponent)
  export default gamePanelCoponent;