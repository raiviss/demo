import { GENERAL_UPDATE } from '../comunicator/actions';

const initialState = {
	text: '',
	options: [] //{id, text}
}

export const encounter = (state = initialState, action) => {
    switch (action.type) {
	  case GENERAL_UPDATE:

	  		if(typeof action.data.encounter !== 'undefined') {
				var newState = action.data.encounter;
				newState.lastUpdate = Date.now();
				return Object.assign({}, state, newState);
			} else {
				return state;
			}
			
		break;
      default:
        return state
    }
}