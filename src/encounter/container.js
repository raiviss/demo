import { connect } from 'react-redux'
import Encounter from '../components/encounter/index'
import { getSocket } from '../comunicator/socket';

const mapStateToProps = (state, ownProps) => {

  return {
	options:state.encounter.options,
	text: state.encounter.text
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    clickHandler: (id) => {

		getSocket().emit('apiMessage', {type: 'POST', method: '/api/encounter-stage/advanceStage',  data: {Id: id}});
    //  
    
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Encounter)