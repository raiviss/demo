import { ADD_MESSAGE, SWITCH_CHAT } from './actions';
import { GENERAL_UPDATE } from '../comunicator/actions';

const initialState = {
    activeChat: 'GLOBAL',
    globalChat: [],
    gangChatEnabled: false,
    gangChat: [],
    sectorChat: [],
    privateChats: [], //{username, userid, messages:[]}
    activePrivateIndx: 0
}

export const chat = (state = initialState, action) => {
    switch (action.type) {
      case GENERAL_UPDATE:

	  		if(typeof action.data.chat !== 'undefined') {
				var newState = action.data.chat;
				newState.lastUpdate = Date.now();
				return Object.assign({}, state, newState);
			} else {
				return state;
			}
			
		break;
      case SWITCH_CHAT:

        return Object.assign({}, state, {activeChat: action.data}); 
      case ADD_MESSAGE: 

        var newstate = {}
        
        switch(action.data.type) {
          case 'GLOBAL':
            newstate.globalChat = [...state.globalChat];
            newstate.globalChat.push(action.data);
            break;
          case 'GANG':
            newstate.gangChat = [...state.gangChat];
            newstate.gangChat.push(action.data);
            break;
          case 'SECTOR':
            newstate.sectorChat = [...state.sectorChat];
            newstate.sectorChat.push(action.data);
            break;
          case 'PRIVATE':

            break;
        }

        return Object.assign({}, state, newstate);
      default:
        return state
    }
  }