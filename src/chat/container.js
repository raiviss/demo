import React from 'react';
import chatComponent from '../components/chat/index';
import { connect } from 'react-redux'
import { chatContext } from '../chatApp/context';
import { getSocket } from '../comunicator/socket';
import { switchChat } from './actions';


const mapStateToProps = (state, ownProps) => {

	const activeChat = state.chat.activeChat;

	switch (activeChat) {
		case 'GLOBAL':
			var messages = state.chat.globalChat;
			break;
		case 'GANG': 
			var messages = state.chat.gangChat;
			break;
		case 'SECTOR': 
			var messages = state.chat.sectorChat;
			break;
		case 'PRIVATE': 
			var messages = state.chat.privateChats[state.chat.activePrivateIndx];
			break;
		default:
			break;
	}

	return {
		privateChats: [],
		activeChat: state.chat.activeChat,
		messages: messages,
		gangChatEnabled: state.chat.gangChatEnabled
	}
}
  
  const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		switchActivehandler: (chatType) => {

			dispatch(switchChat(chatType));
		},
		sendMessage: (text, type) => {
		
			getSocket().emit('chatSendMessage', {type, text});
	//	  dispatch(cancelPath());
  
	  //  
	  
	  }
	}
  }
  
  export default connect(mapStateToProps, mapDispatchToProps, null, { context: chatContext })(chatComponent)
  