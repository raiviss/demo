import { getSocket } from '../comunicator/socket';

export const SWITCH_CHAT = 'SWITCH_CHAT';
export const switchChat = (data) => {

	return {
        type: SWITCH_CHAT,
        data: data
    }
}

export const LOAD_MESSAGES = 'LOAD_MESSAGES';
export const loadMessages = (data) => {

	return {
        type: LOAD_MESSAGES,
        origin: 'front',
        data: data
    }
}

export const ADD_MESSAGE = 'ADD_MESSAGE';

export const sendMessage = () => {

	getSocket().emit('apiMessage', {type: 'PUT', method: '/api/map-path/cancelPath', data: {}});

    return {
        type: CANCEL_PATH_CLICK,
        origin: 'front',
        data:{}
    }
}