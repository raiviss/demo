import { connect } from 'react-redux'
import SquareInfo from '../components/squareInfo/index'
import { map } from '../map/reducer'

const mapStateToProps = (state, ownProps) => {

  if(typeof state.mapUnits.playerPos[0] !== 'undefined' && state.map.sectorsData.length > 0) {
    var yx = state.mapUnits.playerPos[0];
    var coords = state.map.currentPos[1] + '.' + yx[0] + '.' + yx[1];
    var middleSectorIndx = Math.ceil((state.map.sectorsData.length-1) / 2);
    var currentSquare = state.map.sectorsData[middleSectorIndx][middleSectorIndx][yx[0]-1][yx[1]-1];
  } else {
    var coords = '';
    var currentSquare = {rangeDisplay: '', typeDisplay: ''};
  }
  

  

  

  return {
    coords: coords, 
    combatRange: currentSquare.rangeDisplay, 
    areaType: currentSquare.typeDisplay
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SquareInfo)