FROM php:7.2-apache
RUN apt-get update \
    && apt-get install -y \
        vim
RUN a2enmod rewrite